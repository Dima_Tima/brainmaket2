const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const ejs = require('gulp-ejs');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const gulpEjsMonster = require('gulp-ejs-monster');
const sourcemaps = require("gulp-sourcemaps");
const browserSync = require("browser-sync").create();
const flatten = require('gulp-flatten');

function styles (){
    return gulp.src('./src/**/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('/all.css'))
        .pipe(gulp.dest('./public/'))
        .pipe(browserSync.stream());// небыло этой строки-- стрим  

}
gulp.task('styles', styles);

function templates (){
    return gulp.src('./src/**/*.ejs')
        .pipe(gulpEjsMonster({/* plugin options */}))
        .pipe(rename({ extname: '.html'}))
        .pipe(flatten()) /*возвращаем файл без вложений и папок*/
        .pipe(gulp.dest('./public/'))
}
gulp.task('templates', templates);

function images (){
    return gulp.src('src/img/*.png')
        .pipe(imagemin())
        .pipe(gulp.dest('public/images'));
}
gulp.task('images', images);

// watchFiles
function watchFiles() {
    gulp.watch("./src/style/*.sass", styles);
    gulp.watch("./src/templates/**/*.ejs", templates);
    gulp.watch("./public/*.html", browserReload);
    gulp.watch("./public/**/*.css", browserReload);
    gulp.watch("./src/**/*.js", browserReload);
   }

function sync(done) {
    browserSync.init({
     server: {
      baseDir: "./public"
     },
     port: 3000
    });
    done();
   }
   function browserReload(done) {
    browserSync.reload();
   
    done();
   }
   
   gulp.task("default", gulp.parallel(watchFiles, sync));

  //  вызов этого файла, командой gulp (без добовления названий тасок)